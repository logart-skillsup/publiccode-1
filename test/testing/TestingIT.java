/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.util.Calendar;
import java.util.GregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Andr
 */
public class TestingIT extends Assert {

    static Testing t;
    GregorianCalendar gCalendar;

    public TestingIT() {
	System.out.println("designer");
	gCalendar = new GregorianCalendar();

    }

    @BeforeClass
    public static void setUpClass() {
	System.out.println("setUpClass");
	TestingIT.t = new Testing();

    }

    @AfterClass
    public static void tearDownClass() {
	System.out.println("tearDownClass");
    }

    @Before
    public void setUp() {

	System.out.println("setUp");
    }

    @After
    public void tearDown() {

	System.out.println("tearDown");
    }

    @Test
    public void isUnderReturntTime() {
	System.out.println(t.getRandomFloatNumber());
	assertTrue("Date - " + (gCalendar.get(Calendar.DAY_OF_MONTH) + "."
		+ (gCalendar.get(Calendar.MONTH) + 1) + "."
		+ gCalendar.get(Calendar.YEAR)),
		t.getRandomFloatNumber() < 0.5);

    }

    @Test
    public void isUpwardReturnDate() {
	System.out.println(t.getRandomFloatNumber());
	assertFalse("Time - " + (gCalendar.get(Calendar.HOUR_OF_DAY) + "."
		+ gCalendar.get(Calendar.MINUTE) + "."
		+ gCalendar.get(Calendar.SECOND)),
		t.getRandomFloatNumber() < 0.5);

    }
}
